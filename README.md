# Raycaster

An implementation in rust of the raytracer presented in the book [Computer Graphics from Scratch](https://nostarch.com/computer-graphics-scratch).

Usage:

```
$ cargo run -- --width 640 --height 480 data/scenes/scene1.yml

```
![A rendered scene showing 3 colored spheres](image.png)